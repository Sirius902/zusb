# NOTICE
This project has migrated to [a new location](https://github.com/Sirius902/zusb).

# zusb
A port of the library [rusb](https://github.com/a1ien/rusb) to Zig.

## Usage
* Add `libusb-1.0/libusb.h` to the include path in `build.zig`.
* Link libusb 1.0 in `build.zig`.

## TODO
* Add rusb doc comments.

## License
Copyright © 2015 David Cuddeback

Distributed under the [MIT License](LICENSE).
